// cparlma1_final.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "processor.h"
/*references 
	webcam input 
	https://thefreecoder.wordpress.com/2012/09/11/opencv-c-video-capture/
	iris tracker / gaze tracker 
	http://thume.ca/projects/2012/11/04/simple-accurate-eye-center-tracking-in-opencv/
	http://www.inb.uni-luebeck.de/publikationen/pdfs/TiBa11b.pdf
	https://github.com/trishume/eyeLike

*/

#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>
#include <queue>
#include <stdio.h>
#include <math.h>

#include "constants.h"
#include "findEyeCenter.h"
#include "findEyeCorner.h"
#include "PolynomialRegression.h"
#include <vector>
#include <chrono>

/** Constants **/
cv::Point  getEC_IC_Vector(cv::Point2f corner, cv::Rect EyeRegion, cv::Point pupil, char xy) ;

void calculateCoefficients();
void displayGaze(cv::Mat image) ;
void displayError();

void framesPerSecond();
void resetTraining();
void addError(cv::Mat image);

int train(cv::Mat image_in, char c);

cv::Point displayRandomPoint();

cv::Point displayRandomPoint(cv::Mat image);

void trackerdebug(cv::Mat debugFace, cv::Rect rrCornerRegion, cv::Rect lrcornerRegion, cv::Rect rEyeRegion, cv::Rect lEyeRegion, cv::Point rightPupil, cv::Point leftPupil);

cv::Point findEyes(cv::Mat frame_gray, cv::Rect face);

cv::Point detectAndDisplay(cv::Mat frame);

std::vector<int> getPointVals(std::vector<cv::Point> vect, char xy);
void manageQue();
cv::Point averageQue();
cv::Point nextTrainPoint();
cv::Point displayTrainPoint(cv::Mat image_in);


/** Function Headers */
//void detectAndDisplay(cv::Mat frame);

/** Global variables */
//-- Note, either copy these two files from opencv/data/haarscascades to your current folder, or change these locations
cv::String face_cascade_name = "haarcascade_frontalface_alt.xml";
cv::CascadeClassifier face_cascade;
std::string main_window_name = "Capture - Face detection";
std::string face_window_name = "Capture - Face";
cv::RNG rng(12345);
cv::Mat debugImage;
cv::Mat skinCrCbHist = cv::Mat::zeros(cv::Size(256, 256), CV_8UC1);

//my data 
cv::Point pupil_avg;
cv::Point previousPoint(0,0);
float pupil_view_x;
float pupil_view_y;
//cv::Point pupily_avg;
std::string tracker_window_name = "tracker";
cv::Mat tracker_image = cv::Mat::zeros(cv::Size(320, 240), CV_8UC1);
//int clock = 0;
std::string testwindow = "testwindow";
cv::Mat testimage(cv::Size(1600, 1000), CV_8UC3);
cv::Point currentval;
cv::Point Left_ECIvect;
cv::Point Right_ECIvect;
//cv::Point LECIvectY;
cv::Point RECIvectY;

//the values to perform polynomial regression on 
std::vector<cv::Point> randomPoints;
std::vector<cv::Point> ec_ic_right_eye;
std::vector<cv::Point> ec_ic_left_eye;
std::vector<double> Right_x_coef;
std::vector<double> Right_y_coef;
std::vector<double> Left_x_coef;
std::vector<double> Left_y_coef;
int count = 0;
int c = ' ';// = cv::waitKey(0);
cv::Point randp;
cv::Point gazepoint;
std::vector<int> error_vect;
int err = 0;
std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
std::chrono::high_resolution_clock::time_point t2;// = std::chrono::high_resolution_clock::now();
std::chrono::high_resolution_clock::time_point totaltime;
std::chrono::high_resolution_clock::time_point fps;
int framecount = 0;
std::vector<cv::Point> que;
int trainPointit = 0;
std::vector<cv::Point> trainPoints = { cv::Point(10,10), cv::Point(testimage.cols / 2 , 10), cv::Point(testimage.cols - 11,10),
										cv::Point(10,testimage.rows / 2), cv::Point(testimage.cols / 2, testimage.rows / 2), cv::Point(testimage.cols - 11, testimage.rows / 2),
										cv::Point(10, testimage.rows-11), cv::Point(testimage.cols/2,testimage.rows-11), cv::Point(testimage.cols-11, testimage.rows-11) };
/**
* @function main

NOTE 
apply_regression() may need addition argument int y 
also look into reapplying the coefficients 

*/
int main(int argc, const char** argv) {
	srand(0);
	cv::Mat frame;

	// Load the cascades
	if (!face_cascade.load(face_cascade_name)) { printf("--(!)Error loading face cascade, please change face_cascade_name in source code.\n"); return -1; };

	cv::namedWindow(main_window_name, CV_WINDOW_NORMAL);
	cv::moveWindow(main_window_name, 500, 100);
	cv::namedWindow(face_window_name, CV_WINDOW_NORMAL);
	cv::moveWindow(face_window_name, 100, 100);
	cv::namedWindow("Right Eye", CV_WINDOW_NORMAL);
	cv::moveWindow("Right Eye", 200, 650);
	cv::namedWindow("Left Eye", CV_WINDOW_NORMAL);
	cv::moveWindow("Left Eye", 100, 650);
	if(TRAINING)cv::namedWindow(testwindow, CV_WINDOW_AUTOSIZE);
	if(TRAINING)cv::moveWindow(testwindow, 60, 0);
	//cv::namedWindow("aa", CV_WINDOW_NORMAL);
	//cv::moveWindow("aa", 10, 800);
	//cv::namedWindow("aaa", CV_WINDOW_NORMAL);
	//cv::moveWindow("aaa", 10, 800);
	//MYWINDOW 
	if(MINITRACKER)cv::namedWindow(tracker_window_name, CV_WINDOW_NORMAL);
	if(MINITRACKER)cv::moveWindow(tracker_window_name, 500, 650);
	if(RAND)randp = displayRandomPoint(testimage);
	else randp = displayTrainPoint(testimage);

	//randomPoints.push_back(randp);

	createCornerKernels();
	//ellipse(skinCrCbHist, cv::Point(113, 155.6), cv::Size(23.4, 15.2),
	//	43.0, 0.0, 360.0, cv::Scalar(255, 255, 255), -1);

	// I make an attempt at supporting both 2.x and 3.x OpenCV
	//CHECK VERSION AND START CAPTURE 
	#if CV_MAJOR_VERSION < 3	// VERSION
		CvCapture* capture = cvCaptureFromCAM(0);
		if (capture) {				
			while (true) {		
				frame = cvQueryFrame(capture);
	#else						//VERSION
		cv::VideoCapture capture(0);
		if (capture.isOpened()) {
			while (!NOLOOP) {
				capture.read(frame);  
	#endif						//VERSION END 

				// mirror the captured image 
				cv::flip(frame, frame, 1);
				frame.copyTo(debugImage); // copy mirrored image to debugImage 
				imshow(main_window_name, debugImage); //display the debugImage 

				//in calibration mode. wait to capture sample .... realy need to implement state pattern 
				if (count < NUMTRAIN && TRAINING) {
					c = cv::waitKey(0); //accept a keystroke
				}
				
				testimage = cv::Mat::zeros(testimage.size(), testimage.type());//
				//display info onto image after clearing it 
				framesPerSecond();
				displayError();

				t1 = std::chrono::high_resolution_clock::now();
				if (!frame.empty()) {//check if have a frame 
					if (TRAINING && count < NUMTRAIN) { //calibration// training  
						t1 = std::chrono::high_resolution_clock::now();
						if (train(frame, c)) { break; } //train on frame 
						t2 = std::chrono::high_resolution_clock::now();
					}
					else if ((char)c == 'e') { //error capture handler 
						addError(frame);
					}
					else {//testing  mode displays  
						//t1 = std::chrono::high_resolution_clock::now();
						detectAndDisplay(frame);//do normal process 
						//t2 = std::chrono::high_resolution_clock::now();
						displayGaze(testimage);
						//imshow(testwindow, testimage);
					}
				}
				else { //failed to capture frame 
						printf(" --(!) No captured frame -- Break!");
						return 1;
				}
				//calculate coefficients if all samples taken 
				//or if user requests 'r'  
				if (count == NUMTRAIN) {
					count++;
					calculateCoefficients();
					c = 'p';

				}
				t2 = std::chrono::high_resolution_clock::now();


				//get user input 
				if(LOOPPAUSE)
					c = cv::waitKey(250);
				else 
					c = cv::waitKey(1);
				if ((char)c == 'c') {
					resetTraining();
				}
				else if ((char)c == 'q') {
					break;
				}
				
				
			}//end wile loop 
		} // end if capture opened 


		releaseCornerKernels();
		cv::waitKey(0);
		return 0;
}//END MAIN 



//calculate frames per second from global chrono time vals 
void framesPerSecond() {
	std::chrono::seconds sec(1);
	auto ms = std::chrono::microseconds(sec).count();
	auto duration = std::chrono::duration_cast<std::chrono::microseconds>((t2 - t1)).count();
	auto fps = ms / duration;
	//std::cout << fps << ", " << ms << ",  " << duration << std::endl;
	//put frames per second onto test image 
	int ab = fps;
	std::string fps_out = std::to_string(ab);
	cv::putText(testimage, "FPS " + fps_out, cvPoint(30, 30), cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, cv::Scalar(200, 200, 250), 1, CV_AA);
	//imshow(testwindow, testimage);
}




/*
	handle calibration ... capturing samples 
	use either random samples or premade training points 
	insert relative eye position vectors into vectors  
	handle some input 
	display gazepoint 
*/
int train(cv::Mat image_in, char c) {
	if (RAND)
		randomPoints.push_back(displayRandomPoint(testimage));
	else
		randomPoints.push_back(displayTrainPoint(testimage));
	detectAndDisplay(image_in);

	//push back eic eye vectors 
	ec_ic_right_eye.push_back(Right_ECIvect);
	ec_ic_left_eye.push_back(Left_ECIvect);
	count++;
	//accept input 

	if ((char)c == 'q') { return 1; }
	else if ((char)c == 'f') {
		cv::imwrite("frame.png", image_in);
		c = 'p';
	}
	else if ((char)c == ' ') {
		//displayGaze(testimage);
	}
	else if ((char)c == 'r') {
		calculateCoefficients();
		count = NUMTRAIN+1;
		c = 'p';
	}//end reset and display 
	else if ((char)c == 'c') {
		resetTraining();
		c = 'p';
	}
	displayGaze(testimage);
	imshow(testwindow, testimage);
	return 0;
}



/*
display the error values onto the test image 
calculates average of the errors 
Error = error of last error sample capture 
avg Error = running average of errors 
Error rpoint = random point displayed during last error capture
Error gaze   = gazepoint predicted during last error capture
gaze = current gazepoint 
*/
void displayError() {
	int sum_dist_error = 0;
	int avg_dist_error = 0;
	for (int i = 0; i < error_vect.size(); i++) {
		sum_dist_error += error_vect[i];
	}
	if (!error_vect.empty())
		avg_dist_error = sum_dist_error / error_vect.size();
	printf("Error DISTANCE %d\n", avg_dist_error);
	//int distError = sqrt((randp.x - gazepoint.x) ^ 2 + (randp.y - gazepoint.y) ^ 2);
	cv::putText(testimage, "Error " + std::to_string(err), cv::Point(30, 50), CV_FONT_HERSHEY_COMPLEX_SMALL, 0.8, cv::Scalar(200, 200, 250), 1, CV_AA);
	cv::putText(testimage, "avg Error " + std::to_string(avg_dist_error), cv::Point(30, 70), CV_FONT_HERSHEY_COMPLEX_SMALL, 0.8, cv::Scalar(200, 200, 250), 1, CV_AA);
	cv::putText(testimage, "Error rpoint  (" + std::to_string(randp.x) + "," + std::to_string(randp.y) + " )", cv::Point(30, 90), CV_FONT_HERSHEY_COMPLEX_SMALL, 0.8, cv::Scalar(200, 200, 250), 1, CV_AA);
	cv::putText(testimage, "Error gaze  (" + std::to_string(currentval.x) + "," + std::to_string(currentval.y) + " )", cv::Point(30, 110), CV_FONT_HERSHEY_COMPLEX_SMALL, 0.8, cv::Scalar(200, 200, 250), 1, CV_AA);
	cv::putText(testimage, "gaze  (" + std::to_string(gazepoint.x) + "," + std::to_string(gazepoint.y) + " )", cv::Point(30, 130), CV_FONT_HERSHEY_COMPLEX_SMALL, 0.8, cv::Scalar(200, 200, 250), 1, CV_AA);
	cv::imshow(testwindow, testimage);
}


/*	handle collecting error sample data 
	display random point ,   wait for key press
	capture frame and relative eye position vector 
	calculate predicted gaze 

*/
void addError(cv::Mat image_in) {
	testimage = cv::Mat::zeros(testimage.size(), testimage.type());//
	cv::Point ran = displayRandomPoint(testimage); //display random position 
	cv::imshow(testwindow, testimage); //display
	cv::waitKey(0); // wait 
	detectAndDisplay(image_in);//capture // iric center and relative eye position vector 
	displayGaze(testimage); //calc predicted gaze 
	currentval = gazepoint; //use global currentval as the stored error gaze prediction
//	err = sqrt((randp.x - gazepoint.x) ^ 2 + (randp.y - gazepoint.y) ^ 2);
	int diff_x = randp.x - gazepoint.x; //calc euclidean distance error 
	int diff_y = randp.y - gazepoint.y;
	diff_x = diff_x * diff_y;
	diff_y = diff_y *diff_y;
	err = sqrt(diff_x + diff_y);
	if(err > 0)
	error_vect.push_back(err);  //add error to vector of errors ,, used in averageing error 

	//displayGaze(testimage);
	c = 'p';
}

//reset the accumulated sample/training data 
//reset itorators // counters 
void resetTraining() {
	displayError();
	error_vect.clear();
	error_vect.resize(0);
	ec_ic_right_eye.clear();
	ec_ic_right_eye.resize(0);
	ec_ic_left_eye.clear();
	ec_ic_left_eye.resize(0);
	randomPoints.clear();
	randomPoints.resize(0);

	//remove the poly coefficients 
	Right_x_coef.clear();
	Right_y_coef.clear();
	Right_x_coef.resize(0);
	Right_y_coef.resize(0);

	Left_x_coef.clear();
	Left_x_coef.resize(0);
	Left_y_coef.clear();
	Left_y_coef.resize(0);

	trainPointit = 0;
	count = 0;
}


//write and return a random point 
//hardcoded to my screen size :( 
cv::Point displayRandomPoint(cv::Mat image) {
	//clear image 
	//testimage = cv::Mat::zeros(testimage.size(), testimage.type());

	int xrand = rand() % 1600;
	int yrand = rand() % 1000;

	cv::Point randpoint(xrand, yrand);
	circle(image, randpoint, 10, cv::Scalar(40, 160, 40),-1);
	randp = randpoint;
	return randpoint;
}


//write a trainingpoint to and image 
cv::Point displayTrainPoint(cv::Mat image_in) {
	cv::Point next = nextTrainPoint();
	circle(image_in, next, 10, cv::Scalar(40, 160, 40), -1);
	return next;
}



/*
	using predefined training points around outer edge of image 
	return the next training point and increment 
*/
cv::Point nextTrainPoint() {
	cv::Point retval = trainPoints[trainPointit];
	if (trainPointit == trainPoints.size() - 1) 
		trainPointit = 0;
	else 
		trainPointit++;
	return retval;
}



/*
	apply the regression coefficients to each relative position eye vector 
	average the left and right predicted gaze points 
	store the averaged gaze point globaly as gazepoint	
	write circles for each gazepoint to testimage
	right		 = blue 
	left		 = green
	average		 = grey ring
	qued average = baby blue 
	update que with a new gazepoint average 
	display image 
*/
void displayGaze(cv::Mat image_in) {
	if (!Right_x_coef.empty()) {
		//get the screen display coordinates for each eye 
		int right_x_gaze = apply_regression(Right_x_coef, Right_ECIvect.x);
		int right_y_gaze = apply_regression(Right_y_coef, Right_ECIvect.y);
		int left_x_gaze = apply_regression(Left_x_coef, Left_ECIvect.x);
		int left_y_gaze = apply_regression(Left_y_coef, Left_ECIvect.y);
		//average the two points 
		//cv::Point avg(((right_x_gaze + left_x_gaze) / 2) - image_in.cols , ((right_y_gaze + left_y_gaze) / 2) - image_in.rows );
		cv::Point avg(((right_x_gaze + left_x_gaze) / 2)  , ((right_y_gaze + left_y_gaze) / 2)  );
		gazepoint = avg;
		//test print info 
		//printf("Right (%d,%d) \n", right_x_gaze, right_y_gaze);
		//printf("Left  (%d,%d) \n", left_x_gaze, left_y_gaze);
		//printf("avg   (%d,%d) \n", avg.x, avg.y);
		//write circles to the points 		
		cv::circle(image_in, cv::Point(right_x_gaze, right_y_gaze), 4, cv::Scalar(255, 0, 0), -1 );
		cv::circle(image_in, cv::Point(left_x_gaze, left_y_gaze), 4, cv::Scalar(0, 255, 20), -1);
		cv::circle(image_in, avg, 6, cv::Scalar(255, 255, 200));
		//cv::circle(image_in, cv::Point(image_in.cols / 2, image_in.rows / 2), 10, 1234, -1);
		manageQue();
		cv::circle(image_in, averageQue(), 5, cv::Scalar(255, 255, 50), -1 );
		imshow(testwindow, testimage);
	}
}



//average the values in the running que of gaze points/vectors 
//return an averaged point 
cv::Point averageQue() {
	int sum_x = 0;
	int sum_y = 0;
	cv::Point ret(0, 0);
	if (que.size() >= QUESIZE) {
		for (int i = 0; i < que.size(); i++) {
			sum_x += que[i].x;
			sum_y += que[i].y;
		}
	}
	if (que.size() > 0) {
		ret = cv::Point(sum_x / que.size(), sum_y / que.size());
	}
	return ret;
}




//manage the que of previous gaze points
//used to create a running average of gaze points to minimize jitter
//and smooth transitions between frames... this part would require a bit of animation?  
void manageQue() {
	if (que.size() >= QUESIZE) {
		cv::Point tmp = que[1];
		for (int i = 0; i < QUESIZE-1; i++) {
			que[i] = tmp;
			tmp = que[i + 1];
		}
		que[QUESIZE-1] = gazepoint;
		std::cout << que[0] <<  "\t"  << que[1] << "\t" << que[2] << "\t" << que.size() << std::endl;
	}
	else {
		que.push_back(gazepoint);
	}


}



//split the left/right eye vector x/y cordinates.. builds vectors per coordinate
//calls the polynomialRegression function on each new vector
//results stored globaly 
void calculateCoefficients() {
	/*convert / get int vects from vector<Point>
	these are the ec - ic and rand vectors for right eye
	soon to include left eye
	*/
	//SPLIT UP THE VECTORS OF POINTS INTO VECTORS OF X / Y VALUES 
	//random regession vectors 
	std::vector<int> rand_x_vals = getPointVals(randomPoints, 'x');
	std::vector<int> rand_y_vals = getPointVals(randomPoints, 'y');
	//RIGHT EYE REGRESSION VECTORS 
	std::vector<int> REye_x_vals = getPointVals(ec_ic_right_eye, 'x');
	std::vector<int> REye_y_vals = getPointVals(ec_ic_right_eye, 'y');
	//LEFT EYE REGRESSION VECTORS 
	std::vector<int> LEye_x_vals = getPointVals(ec_ic_left_eye, 'x');
	std::vector<int> LEye_y_vals = getPointVals(ec_ic_left_eye, 'y');

	//PERFORM REGRESSIONS , GET VECTOR OF COEFFICIENTS 
	if (true) {
		//right eye 
		Right_x_coef = polynomialRegression(rand_x_vals, REye_x_vals, POLYDEGREES);
		Right_y_coef = polynomialRegression(rand_y_vals, REye_y_vals, POLYDEGREES);
		//left eyes 
		Left_x_coef = polynomialRegression(rand_x_vals, LEye_x_vals, POLYDEGREES);
		Left_y_coef = polynomialRegression(rand_y_vals, LEye_y_vals, POLYDEGREES);
	}
	count = NUMTRAIN;
}



void training(cv::Point randPoint, cv::Point left_ec_ic, cv::Point right_ec_ic) {
}



//get relative position vector, of iris within eye 
//possible source of error. not creating vector "Point" properly, 
//
//get the vector of eye corners - iris centers 
cv::Point  getEC_IC_Vector(cv::Point2f corner , cv::Rect EyeRegion, cv::Point pupil , char xy) {
	cv::Point ec_ic;
	//test different assignments of the return value 
	printf("cor(%d,%d) , eyeReg(%d,%d), pup(%d,%d) \n", corner.x, corner.y, EyeRegion.x, EyeRegion.y, pupil.x, pupil.y);
	//test 1 
		//double ratio_x = ((double)pupil.x - (double)EyeRegion.x) / (double)EyeRegion.width;
		//double ratio_y = ((double)pupil.y - (double)EyeRegion.y) / (double)EyeRegion.height;
		//ec_ic = cv::Point(ratio_x*100.0, ratio_y*100.0);
		//printf("%c  %f , %f test ratio conversion \n", xy, ratio_x, ratio_y);

	//test 2
	//WORKING 
	if (! kEnableEyeCorner) {
		ec_ic = cv::Point(pupil.x - EyeRegion.x, pupil.y - EyeRegion.y);
	}
	else {
		ec_ic = cv::Point(pupil.x - corner.x, pupil.y - corner.y);
	}
	//test 3
	//test 4
		//ec_ic = cv::Point(pupil.x - 2*EyeRegion.x, pupil.y - 2*EyeRegion.y);
	//test 5
		//ec_ic = cv::Point((pupil.x - EyeRegion.x) + testimage.cols/2, (pupil.y - EyeRegion.y) + testimage.rows/2);

	return ec_ic;
}



//non regression 
//shows the small image window with "bullsize" and small moving circle
//display theminitracker window used to get relative eye position mapped to middle of an image 
void trackerdebug(cv::Mat debugFace, cv::Rect rrCornerRegion, cv::Rect lrcornerRegion, cv::Rect rEyeRegion, cv::Rect lEyeRegion, cv::Point rightPupil, cv::Point leftPupil) {
	//debug some circles onto the image 
	cv::Point circlepoint(rrCornerRegion.x, rrCornerRegion.y);
	//circle(debugFace, circlepoint, 5, 1234);

	//get ratio of x in box  and y  on right eye box, 
	double ratx = ((double)rightPupil.x - (double)rEyeRegion.x) / (double)rEyeRegion.width;
	double raty = ((double)rightPupil.y - (double)rEyeRegion.y) / (double)rEyeRegion.height;
	//scale the circle points into the tracker image scale 
	circlepoint.x = ratx*tracker_image.cols;
	circlepoint.y = raty *tracker_image.rows;
	//(circlepoint.x > tracker_image.cols) ? (circlepoint.x = tracker_image.cols - 1) : circlepoint.x;

	//reduce noice by averaging this point with next point 
	circlepoint.x = ((previousPoint.x + circlepoint.x) / 2);
	circlepoint.y = ((previousPoint.y + circlepoint.y) / 2);

	//try to place eye point 
	//draw the circle 
	circle(tracker_image, circlepoint, 5, 1234);
	
	//draw the tracker dot on the small tracker window 
	cv::Point center(tracker_image.cols / 2, tracker_image.rows / 2);
	circle(tracker_image, center, 5, 1234);
	circle(tracker_image, center, 30, 1234);

	//set up previous point to use to average and reduce noise bounce. 
	previousPoint = circlepoint;

	//show circle on tracker
	if(MINITRACKER)imshow(tracker_window_name, tracker_image);

	if (MINITRACKER)tracker_image.setTo(cv::Scalar(0));
}




// test the poly regression algorithm 
void debugRegression() {
	//some testing code 
	std::vector<double> temp{ 80.0, 40.0, -40.0, -120.0, -200.0, -280.0 };
	std::vector<double> alph{ 6.47, 6.24, 5.72, 5.09 ,4.30, 3.33 };

	PolynomialRegression pol;
	if (DEBUGPOLY) {
		pol.testRegression();
		//test poly regression 
		std::vector<int> x{ 1,2,3,4,5 };
		std::vector<int> y{ 1,4,9,16,25 };
		polynomialRegression(x, y, 2);
	}
	else {

	}

}

/*
http://www.inb.uni-luebeck.de/publikationen/pdfs/TiBa11b.pdf
*/
cv::Point findEyes(cv::Mat frame_gray, cv::Rect face) {
	cv::Mat faceROI = frame_gray(face);
	cv::Mat debugFace = faceROI;

	if (kSmoothFaceImage) {
		double sigma = kSmoothFaceFactor * face.width;
		GaussianBlur(faceROI, faceROI, cv::Size(0, 0), sigma);
	}
	//-- Find eye regions and draw them
	int eye_region_width = face.width * (kEyePercentWidth / 100.0);
	int eye_region_height = face.width * (kEyePercentHeight / 100.0);
	int eye_region_top = face.height * (kEyePercentTop / 100.0);
	cv::Rect leftEyeRegion(face.width*(kEyePercentSide / 100.0),
		eye_region_top, eye_region_width, eye_region_height);
	cv::Rect rightEyeRegion(face.width - eye_region_width - face.width*(kEyePercentSide / 100.0),
		eye_region_top, eye_region_width, eye_region_height);

	//-- Find Eye Centers
	cv::Point leftPupil = findEyeCenter(faceROI, leftEyeRegion, "Left Eye");
	cv::Point rightPupil = findEyeCenter(faceROI, rightEyeRegion, "Right Eye");
	// get corner regions
	cv::Rect leftRightCornerRegion(leftEyeRegion);
	leftRightCornerRegion.width -= leftPupil.x;
	leftRightCornerRegion.x += leftPupil.x;
	leftRightCornerRegion.height /= 2;
	leftRightCornerRegion.y += leftRightCornerRegion.height / 2;
	cv::Rect leftLeftCornerRegion(leftEyeRegion);
	leftLeftCornerRegion.width = leftPupil.x;
	leftLeftCornerRegion.height /= 2;
	leftLeftCornerRegion.y += leftLeftCornerRegion.height / 2;
	cv::Rect rightLeftCornerRegion(rightEyeRegion);
	rightLeftCornerRegion.width = rightPupil.x;
	rightLeftCornerRegion.height /= 2;
	rightLeftCornerRegion.y += rightLeftCornerRegion.height / 2;
	cv::Rect rightRightCornerRegion(rightEyeRegion);
	rightRightCornerRegion.width -= rightPupil.x;
	rightRightCornerRegion.x += rightPupil.x;
	rightRightCornerRegion.height /= 2;
	rightRightCornerRegion.y += rightRightCornerRegion.height / 2;
	rectangle(debugFace, leftRightCornerRegion, 200);
	rectangle(debugFace, leftLeftCornerRegion, 200);
	rectangle(debugFace, rightLeftCornerRegion, 200);
	rectangle(debugFace, rightRightCornerRegion, 200);

	//SET UP THE CURRENT VAL FOR TETSING THE POLYNOMIAL REDUCTION 
	//currentval = rightPupil;
	// change eye centers to face coordinates
	rightPupil.x += rightEyeRegion.x;
	rightPupil.y += rightEyeRegion.y;
	leftPupil.x += leftEyeRegion.x;
	leftPupil.y += leftEyeRegion.y;
	// draw eye centers
	circle(debugFace, rightPupil, 3, 1234);
	circle(debugFace, leftPupil, 3, 1234);

	//-- Find Eye Corners
	if (kEnableEyeCorner) {
		cv::Point2f leftRightCorner = findEyeCorner(faceROI(leftRightCornerRegion), true, false);
		leftRightCorner.x += leftRightCornerRegion.x;
		leftRightCorner.y += leftRightCornerRegion.y;
		cv::Point2f leftLeftCorner = findEyeCorner(faceROI(leftLeftCornerRegion), true, true);
		leftLeftCorner.x += leftLeftCornerRegion.x;
		leftLeftCorner.y += leftLeftCornerRegion.y;
		cv::Point2f rightLeftCorner = findEyeCorner(faceROI(rightLeftCornerRegion), false, true);
		rightLeftCorner.x += rightLeftCornerRegion.x;
		rightLeftCorner.y += rightLeftCornerRegion.y;
		cv::Point2f rightRightCorner = findEyeCorner(faceROI(rightRightCornerRegion), false, false);
		rightRightCorner.x += rightRightCornerRegion.x;
		rightRightCorner.y += rightRightCornerRegion.y;
		circle(faceROI, leftRightCorner, 3, 200);
		circle(faceROI, leftLeftCorner, 3, 200);
		circle(faceROI, rightLeftCorner, 3, 200);
		circle(faceROI, rightRightCorner, 3, 200);

		Right_ECIvect= getEC_IC_Vector(rightLeftCorner, rightEyeRegion, rightPupil , 'x');
		Left_ECIvect = getEC_IC_Vector(leftLeftCorner, leftEyeRegion, leftPupil, 'x');
	}

	else {
		Right_ECIvect= getEC_IC_Vector(cv::Point2f(0,0), rightEyeRegion, rightPupil , 'x');
		Left_ECIvect = getEC_IC_Vector(cv::Point2f(0,0), leftEyeRegion, leftPupil, 'x');

	}

	//call the tracker	
	if(MINITRACKER)
		trackerdebug(debugFace, rightRightCornerRegion, leftRightCornerRegion, rightEyeRegion, leftEyeRegion, rightPupil, leftPupil);

	//cv::Point  getEC_IC_Vector(cv::Rect CornerRegion, cv::Rect EyeRegion, cv::Point pupil) {
	//Right_ECIvect= getEC_IC_Vector(rightRightCornerRegion, rightEyeRegion, rightPupil , 'x');
	//Left_ECIvect = getEC_IC_Vector(leftRightCornerRegion, leftEyeRegion, leftPupil, 'x');


	//RECIvectY = getEC_IC_Vector(rightRightCornerRegion, rightEyeRegion, rightPupil , 'y');
    //LECIvectY = getEC_IC_Vector(leftRightCornerRegion, leftEyeRegion, leftPupil, 'y');
	//cv::Point leftEC_IC;
	//currentval = Right_ECIvect;

	//return vector of points consisting of right eyes EC-IC vector ,  
	//SET UP THE CURRENT VAL FOR TETSING THE POLYNOMIAL REDUCTION 

	imshow(face_window_name, faceROI);
	//  cv::Rect roi( cv::Point( 0, 0 ), faceROI.size());
	//  cv::Mat destinationROI = debugImage( roi );
	//  faceROI.copyTo( destinationROI );
	return Right_ECIvect;
}




/*
http://www.inb.uni-luebeck.de/publikationen/pdfs/TiBa11b.pdf
*/
cv::Mat findSkin(cv::Mat &frame) {
	cv::Mat input;
	cv::Mat output = cv::Mat(frame.rows, frame.cols, CV_8U);

	cvtColor(frame, input, CV_BGR2YCrCb);

	for (int y = 0; y < input.rows; ++y) {
		const cv::Vec3b *Mr = input.ptr<cv::Vec3b>(y);
		//    uchar *Or = output.ptr<uchar>(y);
		cv::Vec3b *Or = frame.ptr<cv::Vec3b>(y);
		for (int x = 0; x < input.cols; ++x) {
			cv::Vec3b ycrcb = Mr[x];
			//      Or[x] = (skinCrCbHist.at<uchar>(ycrcb[1], ycrcb[2]) > 0) ? 255 : 0;
			if (skinCrCbHist.at<uchar>(ycrcb[1], ycrcb[2]) == 0) {
				Or[x] = cv::Vec3b(0, 0, 0);
			}
		}
	}
	return output;
}

/**
* @function detectAndDisplay
http://www.inb.uni-luebeck.de/publikationen/pdfs/TiBa11b.pdf
modified ? 
*/
cv::Point detectAndDisplay(cv::Mat frame) {
	std::vector<cv::Rect> faces;
	//cv::Mat frame_gray;

	std::vector<cv::Mat> rgbChannels(3);
	cv::split(frame, rgbChannels);
	cv::Mat frame_gray = rgbChannels[2];

	//cvtColor( frame, frame_gray, CV_BGR2GRAY );
	//equalizeHist( frame_gray, frame_gray );
	//cv::pow(frame_gray, CV_64F, frame_gray);
	//-- Detect faces
	face_cascade.detectMultiScale(frame_gray, faces, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE | CV_HAAR_FIND_BIGGEST_OBJECT, cv::Size(150, 150));
	//findSkin(debugImage);

	for (int i = 0; i < faces.size(); i++)
	{
		rectangle(debugImage, faces[i], 1234);
	}
	//-- Show what you got
	cv::Point retvect;
	if (faces.size() > 0) {
		retvect = findEyes(frame_gray, faces[0]);
	}
	//return a ec-ic value 
	return retvect;
}


/*
http://www.inb.uni-luebeck.de/publikationen/pdfs/TiBa11b.pdf
*/
std::vector<int> getPointVals(std::vector<cv::Point> vect, char xy) {
	std::vector<int> ret;
	for (int i = 0; i < vect.size(); i++) {
		if (xy = 'x') {
			ret.push_back(vect[i].x);
		}
		else {
			ret.push_back(vect[i].y);
		}
	}
	return ret;

}




/*

		//accept keyboard keys q= quit , c = capture still frame.  
		if (c == 'q')break;
		else if (c == 'c') {
			Mat im = cameraFrame.clone();
			imshow("dis",im);
		}

		*/


		// Get the horizontal and vertical screen sizes in pixel
/*
using namespace std;
using namespace cv;
void GetDesktopResolution(int& horizontal, int& vertical)
{
	Rect desktop;
	// Get a handle to the desktop window
	const HWND hDesktop = GetDesktopWindow();
	// Get the size of screen to the variable desktop
	GetWindowRect(hDesktop, &desktop);
	// The top left corner will have coordinates (0,0)
	// and the bottom right corner will have coordinates
	// (horizontal, vertical)
	horizontal = desktop.right;
	vertical = desktop.bottom;
}
*/


/*
	hough Circle Demo
	https://github.com/opencv/opencv/blob/master/samples/cpp/tutorial_code/ImgTrans/HoughCircle_Demo.cpp
*/
/*
namespace {

	const std::string windowName = "Hough Circle Detection Demo";
	const std::string cannyThresholdTrackbarName = "Canny threshold";
	const std::string accumulatorThresholdTrackbarName = "Accumulator Threshold";
	const std::string usage = "Usage : tutorial_HoughCircle_Demo <path_to_input_image>\n";

	// initial and max values of the parameters of interests.
	const int cannyThresholdInitialValue = 100;
	const int accumulatorThresholdInitialValue = 50;
	const int maxAccumulatorThreshold = 200;
	const int maxCannyThreshold = 255;
	std::vector<Vec3f> HoughDetection(const Mat& src_gray, const Mat& src_display, int cannyThreshold, int accumulatorThreshold)
	{
		// will hold the results of the detection
		std::vector<Vec3f> circles;
		// runs the actual detection
		HoughCircles(src_gray, circles, HOUGH_GRADIENT, 1, src_gray.rows / 8, cannyThreshold, accumulatorThreshold, 0, 0);

		// clone the colour, input image for displaying purposes
		Mat display = src_display.clone();
		for (size_t i = 0; i < circles.size(); i++)
		{
			Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
			int radius = cvRound(circles[i][2]);
			// circle center
			circle(display, center, 3, Scalar(0, 255, 0), -1, 8, 0);
			// circle outline
			circle(display, center, radius, Scalar(0, 0, 255), 3, 8, 0);
		}
		// shows the results
		imshow("dis", display);
		return circles;
	}
}

*/

/*
	hough circle demo (Main) and video capture. 
*/

/*
		//create video capture stream .. check if opened 
VideoCapture stream(0);
if (!stream.isOpened()) {
	cout << "cannot open camera\n";
}

//start reading and displaying video 
while (true) {
	Mat cameraFrame;
	stream.read(cameraFrame);
	//attempt to mess with frames 
	for (int i = cameraFrame.cols / 4; i < cameraFrame.cols * 3 / 4; i++) {
		for (int x = 0; x < cameraFrame.rows; x++) {
			cameraFrame.at<uchar>(i, x) == cameraFrame.at<uchar>(i, x) - 255;
		}
	}



	//conver the image to gray
	Mat src_gray;
	cvtColor(cameraFrame, src_gray, CV_BGR2GRAY);

	//reduce noise to reduce false circle detection
	GaussianBlur(src_gray, src_gray, Size(9, 9), 2, 2);
	vector<Vec3f> circles;

	int cannyThreshold = cannyThresholdInitialValue;
	int accumulatorThreshold = accumulatorThresholdInitialValue;

	circles = HoughDetection(src_gray, cameraFrame, cannyThreshold, accumulatorThreshold);
	int num_circles = circles.size();
	//Apply the hough Transform to find the circles
	//HoughCircles(src_gray, circles, CV_HOUGH_GRADIENT, 1, src_gray.rows / 8, 200, 100, 0, 0);

	for (size_t i = 0; i < circles.size(); i++)
	{
		Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
		int radius = cvRound(circles[i][2]);
		// circle center
		circle(cameraFrame, center, 3, Scalar(0, 255, 0), -1, 8, 0);
		// circle outline
		circle(cameraFrame, center, radius, Scalar(0, 0, 255), 3, 8, 0);
	}




	//accept keyboard keys q= quit , c = capture still frame.  
	//display the frames 
	//imshow("cam", cameraFrame);



	char c = waitKey(30);
	//capture frame 
	if (c == 'q')break;
	else if (c == 'c') {
		Mat im = cameraFrame.clone();
		imshow("dis2", im);
	}
	else if (num_circles > 1) {
		Mat im = cameraFrame.clone();
		imshow("dis2", im);
	}



}
*/



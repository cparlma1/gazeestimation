#ifndef CONSTANTS_H
#define CONSTANTS_H



// poly regression
#define QUESIZE  2
#define POLYDEGREES 2
#define NUMTRAIN 100
#define DEBUGPOLY 0

//displays 
#define DEBUGIMAGES 0
#define MINITRACKER 1

//controlflow 
#define NOLOOP 0
#define LOOPITOR 1
#define TRAINING 1 
#define LOOPPAUSE 0
#define ERRORCALC 1
//training points 
#define RAND 0



// Debugging
const bool kPlotVectorField = false;

// Size constants
const int kEyePercentTop = 31; //25
const int kEyePercentSide = 20; // 13
const int kEyePercentHeight = 15; //20
const int kEyePercentWidth = 24; //35

// Preprocessing
const bool kSmoothFaceImage = true;
const float kSmoothFaceFactor = 0.005;//.005

// Algorithm Parameters
const int kFastEyeWidth = 50;
const int kWeightBlurSize = 5;
const bool kEnableWeight = true;
const float kWeightDivisor = 1.0;
const double kGradientThreshold = 50.0;

// Postprocessing
const bool kEnablePostProcess = true;
const float kPostProcessThreshold = 0.97;

// Eye Corner
const bool kEnableEyeCorner = false; //broken? 

#endif

//opencv info mach learning ? 
//https://docs.opencv.org/3.1.0/dc/dd6/ml_intro.html
//polyn reg
//http://www.bragitoff.com/2015/09/c-program-for-polynomial-fit-least-squares/
//data to test poly reg
//https://www.google.com/search?q=coordinate+polynomial+regression&ie=utf-8&oe=utf-8&client=firefox-b-ab
//paper 1 eye gaze tracking 
//https://arxiv.org/pdf/1605.05272.pdf
//paper 2 eye gaze tracking 
//http://cseweb.ucsd.edu/~tiqbal/Papers/Tariq_Iqbal_Final_MS_Thesis.pdf
//Polynomial Fit
#include "stdafx.h"

#include "PolynomialRegression.h"
using namespace std;

//http://www.bragitoff.com/2015/09/c-program-for-polynomial-fit-least-squares/

int PolynomialRegression::testRegression() {
	int i, j, k, n, N;

	cout.precision(4);                        //set precision
	cout.setf(ios::fixed);
	//get input 
	cout << "\nEnter the no. of data pairs to be entered:\n";        //To find the size of arrays that will store x,y, and z values
	cin >> N;
	//declare the x and y values 	
	vector<double> x(N), y(N); //set there size ? .reserve(N)? 

	cout << "\nEnter the x-axis values:\n";                //Input x-values
	for (i = 0; i<N; i++)
		cin >> x[i];
	cout << "\nEnter the y-axis values:\n";                //Input y-values
	for (i = 0; i<N; i++)
		cin >> y[i];
	cout << "\nWhat degree of Polynomial do you want to use for the fit?\n";
	cin >> n;                                // n is the degree of Polynomial 

	vector<double> Xsig(2*n+1); // set the size? 
	for (i = 0; i<2 * n + 1; i++)
	{
		Xsig[i] = 0;
		for (j = 0; j<N; j++)
			Xsig[i] = Xsig[i] + pow(x[j], i);        //consecutive positions of the array will store N,sigma(xi),sigma(xi^2),sigma(xi^3)....sigma(xi^2n)
	}

	//double B[n + 1][n + 2], a[n + 1];            //B is the Normal matrix(augmented) that will store the equations, 'a' is for value of the final coefficients
//	vector< vector<int>> B;
	const int bc = n;
	vector<vector<double> > B((n + 1), vector<double>(n + 2));
	vector<double> a(n + 1);

	for (i = 0; i <= n; i++)
	for (j = 0; j <= n; j++)
	B[i][j] = Xsig[i + j];            //Build the Normal matrix by storing the corresponding coefficients at the right positions except the last column of the matrix
	//double Y[ n + 1];                    //Array to store the values of sigma(yi),sigma(xi*yi),sigma(xi^2*yi)...sigma(xi^n*yi)
	vector<double> Y(n + 1);
	for (i = 0; i<n + 1; i++)
	{
		Y[i] = 0;
		for (j = 0; j<N; j++)
			Y[i] = Y[i] + pow(x[j], i)*y[j];        //consecutive positions will store sigma(yi),sigma(xi*yi),sigma(xi^2*yi)...sigma(xi^n*yi)
	}


	for (i = 0; i <= n; i++)
		B[i][n + 1] = Y[i];                //load the values of Y as the last column of B(Normal Matrix but augmented)
	n = n + 1;                //n is made n+1 because the Gaussian Elimination part below was for n equations, but here n is the degree of polynomial and for n degree we get n+1 equations
	cout << "\nThe Normal(Augmented Matrix) is as follows:\n";
	for (i = 0; i<n; i++)            //print the Normal-augmented matrix
	{
		for (j = 0; j <= n; j++)
			cout << B[i][j] << setw(16);
		cout << "\n";
	}



	for (i = 0; i<n; i++)                    //From now Gaussian Elimination starts(can be ignored) to solve the set of linear equations (Pivotisation)
		for (k = i + 1; k<n; k++)
			if (B[i][i]<B[k][i])
				for (j = 0; j <= n; j++)
				{
					double temp = B[i][j];
					B[i][j] = B[k][j];
					B[k][j] = temp;
				}

	for (i = 0; i<n - 1; i++)            //loop to perform the gauss elimination
		for (k = i + 1; k<n; k++)
		{
			double t = B[k][i] / B[i][i];
			for (j = 0; j <= n; j++)
				B[k][j] = B[k][j] - t*B[i][j];    //make the elements below the pivot elements equal to zero or elimnate the variables
		}


	for (i = n - 1; i >= 0; i--)                //back-substitution
	{                        //x is an array whose values correspond to the values of x,y,z..
		a[i] = B[i][n];                //make the variable to be calculated equal to the rhs of the last equation
		
		for (j = 0; j<n; j++)
			if (j != i)            //then subtract all the lhs values except the coefficient of the variable whose value                                   is being calculated
				a[i] = a[i] - B[i][j] * a[j];
		a[i] = a[i] / B[i][i];            //now finally divide the rhs by the coefficient of the variable to be calculated
	}
	cout << "\nThe values of the coefficients are as follows:\n";
	for (i = 0; i<n; i++)
		cout << "x^" << i << "=" << a[i] << endl;            // Print the values of x^0,x^1,x^2,x^3,....    
	cout << "\nHence the fitted Polynomial is given by:\ny=";
	for (i = 0; i<n; i++)
		cout << " + (" << a[i] << ")" << "x^" << i;
	cout << "\n";
	return 0;
}


std::vector<double> polynomialRegression(std::vector<int> ec_ic , std::vector<int> randoms, int n) {
	//check sizes equal 
	if (ec_ic.size() != randoms.size()) { printf("polyRegression Errror, Size mismatch\t(%d, %d)\n",ec_ic.size(), randoms.size()); return std::vector<double>(); }

	// set N, the number of values to regress on .
	int N = ec_ic.size();

	//convert the integer vectors.
	std::vector<double> x(ec_ic.begin(), ec_ic.end());
	std::vector<double> y(randoms.begin(), randoms.end());

	int i , j, k;
	// begin regression 
	vector<double> Xsig(2 * n + 1); // set the size? 
	for (int i = 0; i<2 * n + 1; i++)
	{
		Xsig[i] = 0;
		for (j = 0; j<N; j++)
			Xsig[i] = Xsig[i] + pow(x[j], i);        //consecutive positions of the array will store N,sigma(xi),sigma(xi^2),sigma(xi^3)....sigma(xi^2n)
	}

	//double B[n + 1][n + 2], a[n + 1];            //B is the Normal matrix(augmented) that will store the equations, 'a' is for value of the final coefficients
	//	vector< vector<int>> B;
	const int bc = n;
	vector<vector<double> > B((n + 1), vector<double>(n + 2));
	vector<double> a(n + 1);

	for (i = 0; i <= n; i++)
		for (j = 0; j <= n; j++)
			B[i][j] = Xsig[i + j];            //Build the Normal matrix by storing the corresponding coefficients at the right positions except the last column of the matrix
											  //double Y[ n + 1];                    //Array to store the values of sigma(yi),sigma(xi*yi),sigma(xi^2*yi)...sigma(xi^n*yi)
	vector<double> Y(n + 1);
	for (i = 0; i<n + 1; i++)
	{
		Y[i] = 0;
		for (j = 0; j<N; j++)
			Y[i] = Y[i] + pow(x[j], i)*y[j];        //consecutive positions will store sigma(yi),sigma(xi*yi),sigma(xi^2*yi)...sigma(xi^n*yi)
	}


	for (i = 0; i <= n; i++)
		B[i][n + 1] = Y[i];                //load the values of Y as the last column of B(Normal Matrix but augmented)
	n = n + 1;                //n is made n+1 because the Gaussian Elimination part below was for n equations, but here n is the degree of polynomial and for n degree we get n+1 equations
	cout << "\nThe Normal(Augmented Matrix) is as follows:\n";
	for (i = 0; i<n; i++)            //print the Normal-augmented matrix
	{
		for (j = 0; j <= n; j++)
			cout << B[i][j] << setw(16);
		cout << "\n";
	}



	for (i = 0; i<n; i++)                    //From now Gaussian Elimination starts(can be ignored) to solve the set of linear equations (Pivotisation)
		for (k = i + 1; k<n; k++)
			if (B[i][i]<B[k][i])
				for (j = 0; j <= n; j++)
				{
					double temp = B[i][j];
					B[i][j] = B[k][j];
					B[k][j] = temp;
				}

	for (i = 0; i<n - 1; i++)            //loop to perform the gauss elimination
		for (k = i + 1; k<n; k++)
		{
			double t = B[k][i] / B[i][i];
			for (j = 0; j <= n; j++)
				B[k][j] = B[k][j] - t*B[i][j];    //make the elements below the pivot elements equal to zero or elimnate the variables
		}


	for (i = n - 1; i >= 0; i--)                //back-substitution
	{                        //x is an array whose values correspond to the values of x,y,z..
		a[i] = B[i][n];                //make the variable to be calculated equal to the rhs of the last equation

		for (j = 0; j<n; j++)
			if (j != i)            //then subtract all the lhs values except the coefficient of the variable whose value                                   is being calculated
				a[i] = a[i] - B[i][j] * a[j];
		a[i] = a[i] / B[i][i];            //now finally divide the rhs by the coefficient of the variable to be calculated
	}
	cout << "\nThe values of the coefficients are as follows:\n";
	for (i = 0; i<n; i++)
		cout << "x^" << i << "=" << a[i] << endl;            // Print the values of x^0,x^1,x^2,x^3,....    
	cout << "\nHence the fitted Polynomial is given by:\ny=";
	for (i = 0; i<n; i++)
		cout << " + (" << a[i] << ")" << "x^" << i;
	cout << "\n";
		
	
	return a;
}



/*
screenXi = a0x1 + a1y1 + a2xiyi + a3xi2 + a4yi2 + a5
screenYi = b0x1 + b1y1 + b2xiyi + b3xi2 + b4yi2 + b5
*/
int apply_regression(std::vector<double> val , int x) {
	int ret = 0;

	for (int i = 0; i < val.size(); i++) {
		ret += ((int)val[i] * x ^ i);
	}

	
	printf("SIZE OF COEFFICIENTS VECOT : : %d\n", val.size());



	return ret;
}

/*

//output attached as .jpg
*/